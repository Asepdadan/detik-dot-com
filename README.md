## How To Use
Database: mysql

1. composer install from your root project
2. ensure your connection, username, password, host
   - variable $database will replace after run create database on cli
3. create database anything name
    ```sh=
    php src/database-cli.php detik_com
    ```
4. running on your terminal and current on root project
    ```sh=
    php src/migrations/invoice_payment_migrations.php
    ```

5. open your postman or anything client tools
    - POST create transaction
    - GET check status

6. running transaction-cli for update status transaction
    ```sh=
    php src/transaction-cli.php {references_id} {status}

    php src/transaction-cli.php 20230416170426 paid
    ```


notes:
1. import file environment on postman filename => Detik Dot Com.postman_environment.json
2. import file collection on postman filename => Detik Dot Com.postman_collection.json