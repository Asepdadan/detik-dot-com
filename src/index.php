<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once "route.php";
require_once "../vendor/autoload.php";
require_once './config/database.php';


header('Content-type: application/json');

$api = new \Asdan\DetikPhpNative\Route();
$api->execute();