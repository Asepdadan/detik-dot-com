<?php

namespace Asdan\DetikPhpNative;

use mysqli;

class Database
{
    public $mysqli, $error;
    private $host = 'localhost';
    private $user = 'lost';
    private $password = 'Lost100%';
    private $database = "detik_com";
   public function __construct()
    {
        $this->mysqli = new mysqli($this->host, $this->user, $this->password, $this->database);

        if ($this->mysqli->connect_error) {
            die('Connection error: ' . $this->mysqli->connect_error);
        }
    }

    public function close()
    {
        $this->mysqli->close();
    }

    public function query($sql)
    {
        if ($this->mysqli->query($sql) === false) {
            $this->error = $this->mysqli->error;
            die('Query error: ' . $this->mysqli->error);
        }
    }


    public function escape($value)
    {
        return $this->mysqli->real_escape_string($value);
    }

    public function createTable($tableName, $columns)
    {
        $sql = "CREATE TABLE $tableName (";

        foreach ($columns as $columnName => $columnType) {
            $sql .= "$columnName $columnType, ";
        }

        $sql = rtrim($sql, ', ');
        $sql .= ')';

        $this->query($sql);
    }
}