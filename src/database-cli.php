<?php
use Asdan\DetikPhpNative\Database;

require_once 'config/database.php';

echo "\n============================Start Create Database CLI=====================================\n";

try {
    $databaseName = isset($argv[1]) ? $argv[1] : null;
    if (!$databaseName) {
        throw new Exception("Usage: php database-cli.php {database_name} \n");
    }

    // $conn = new Database();
    // $sql = "CREATE DATABASE {$databaseName}";
    // $valid = $conn->mysqli->query($sql);

    $file = fopen($_SERVER['DOCUMENT_ROOT'].'src/config/database.php', 'r+');
    echo $file;
    // Baca file baris per baris dan cari baris kode yang ingin diganti
    $line_number = 0;
    while (!feof($file)) {
        $line_number++;
        $line = fgets($file);
        // echo $line."\n";
        // Pengecekan pada baris kode tertentu
        if ($line_number == 13) {
            // Lakukan penggantian string pada baris kode yang ditentukan
            $new_line = str_replace("example", "{$databaseName}", (string)$line);
            // echo $new_line;
            // echo (string)$line;
            fseek($file, strlen($line) * -1, SEEK_CUR);
            fwrite($file, $new_line);
        }
    }

    // Tutup file
    fclose($file);

    echo "successfull creating database";
} catch (\Throwable $th) {
    echo "{$th->getMessage()}";
}

echo "\n============================End Create Database CLI=======================================\n";
