<?php
use Asdan\DetikPhpNative\Database;

require_once 'config/database.php';

echo "\n============================Start Transaction CLI=====================================\n";

$referencesId = isset($argv[1]) ? $argv[1] : null;
$status = isset($argv[2]) ? $argv[2] : null;

try {

    if (!$referencesId || !$status) {
        throw new Exception("Usage: php transaction-cli.php {references_id} {status}\n");
    }

    $validStatus = array('pending', 'paid', 'failed');
    if (!in_array($status, $validStatus)) {
        throw new Exception("Transaction not valid argument 2");
    }

    $conn   = new Database();
    $sql = "SELECT * FROM invoice_payment WHERE references_id = '$referencesId'";
    $valid = $conn->mysqli->query($sql);

    if ( empty($valid->fetch_array(MYSQLI_NUM))  ) {
        throw new Exception("Transaction with references ID {$referencesId} not valid \n, because not registered on database");
    }

    $sql    = "UPDATE invoice_payment SET status = '$status' WHERE references_id = '$referencesId'";
    $result = $conn->mysqli->query($sql);
    echo "Transaction with references ID {$referencesId} has been updated to status {$status}\n";
} catch (\Throwable $th) {
    echo "{$th->getMessage()}";
}

echo "\n============================End Transaction CLI=======================================\n";
