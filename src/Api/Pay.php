<?php

namespace Asdan\DetikPhpNative\Api;

use Asdan\DetikPhpNative\Api\Models\InvoicePayment;
use Asdan\DetikPhpNative\Api\Validations\PayValidation;
use Asdan\DetikPhpNative\Database;

class Pay
{
    private $data, $conn, $mysqli;
    public function __construct($data)
    {
        $this->data = $data;
        $conn = new Database();
        $this->conn = $conn;
        $this->mysqli = $conn->mysqli;
    }

    public function createTransaction()
    {
        $data      = $this->data;
        $validated = new PayValidation($data);
        $validated = $validated->validated();

        try {
            $invoice_id    = $this->conn->escape($validated->invoice_id);
            $item_name     = $this->conn->escape($validated->item_name);
            $amount        = $this->conn->escape(str_replace(",", ".", str_replace(".", "", $validated->amount)));
            $payment_type  = $this->conn->escape($validated->payment_type); //(virtual_account, credit_card)
            $customer_name = $this->conn->escape($validated->customer_name);
            $merchant_id   = $this->conn->escape($validated->merchant_id);
            $status        = "pending";
            $references_id = date('YmdHis');
            $number_va     = rand(pow(10, 16 - 1), pow(10, 16) - 1);

            $sql    = "INSERT INTO invoice_payment (invoice_id, references_id, item_name, amount, payment_type, customer_name, merchant_id, status) VALUES
                    ('$invoice_id', '$references_id', '$item_name', '$amount', '$payment_type', '$customer_name', '$merchant_id', '$status')";
            $result = $this->conn->query($sql);

            echo json_encode([
                'status' => true,
                'references_id' => $references_id,
                'number_va' => $number_va
            ]);
        } catch (\Throwable $th) {
            echo json_encode([
                'status' => false,
                'message' => "error ".$th->getMessage()
            ]);
        }

        exit();
    }

    public function checkStatus()
    {
        $references_id = $this->data->references_id;
        $merchant_id   = $this->data->merchant_id;

        $sql = "SELECT * FROM invoice_payment where merchant_id = '$merchant_id' and references_id = '$references_id'";

        $result = $this->mysqli->query($sql);
        $data = $result->fetch_array(MYSQLI_ASSOC);

        if (!empty($data)) {
            $data['amount'] = "Rp".number_format($data["amount"], 2, ',', '.');
        }

        echo json_encode([
            'status' => true,
            'data' => $data ?? []
        ]);
    }
}
