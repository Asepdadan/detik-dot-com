<?php

namespace Asdan\DetikPhpNative\Api\Validations\Rules;

class RequiredRule
{
    private $value;
    public function __construct($value)
    {
        $this->value = $value;
    }

    public function valid()
    {
        if (! empty($this->value)) {
            return [true, $this->value, ""];
        }

        return [false, $this->value, "is not string"];
    }

}