<?php

namespace Asdan\DetikPhpNative\Api\Validations\Rules;

class IntegerRule
{
    private $value;
    public function __construct($value)
    {
        $this->value = $value;
    }

    public function valid()
    {
        if (is_integer($this->value)) {
            return [true, $this->value, ""];
        }

        return [false, $this->value, "you must enter value is integer"];
    }

}