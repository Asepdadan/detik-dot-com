<?php

namespace Asdan\DetikPhpNative\Api\Validations;

class Base
{
    private $request;
    public function __construct($request)
    {
        $this->request = $request;
    }

    public function validated()
    {
        $rules        = $this->rules();
        $validRequest = [];
        $messages     = [];

        foreach ($rules as $key => $rule) {
            $splits = explode("|", $rule);
            $countValidRule = 0;

            foreach ($splits as $keySplit => $split) {
                $value         = $this->request->$key;
                $className     = ucfirst($split);
                $class         = ["Asdan", "DetikPhpNative", "Api", "Validations", "Rules", $className . "Rule"];
                $classInstance = implode("\\", $class);

                if (class_exists($classInstance)) {
                    $instance = new $classInstance($value);
                    [$valid, $value, $message] = $instance->valid();

                    if ($valid ) {
                        $countValidRule += 1;
                    } else {
                        $messages[$key][] = $message;
                    }
                }
            }

            if ($countValidRule == sizeof($splits)) {
                $validRequest[$key] = $value;
            }
        }

        $valid = count((array) $this->request) == count((array)$validRequest);

        if ($valid) {
            return (object)$validRequest;
        }

        echo json_encode($messages);
        die;
    }

}