<?php

namespace Asdan\DetikPhpNative\Api\Validations;

use Asdan\DetikPhpNative\Db;

class PayValidation extends Base
{

    public function rules()
    {
        return [
            'invoice_id' => 'required|integer',
            'item_name' => 'required|string',
            'amount' => 'required',
            'payment_type' => 'required',
            'customer_name' => 'required|string',
            'merchant_id' => 'required|string',
        ];
    }

}