<?php
use Asdan\DetikPhpNative\Database;

require_once $_SERVER['DOCUMENT_ROOT'] . 'src/config/database.php';
require_once $_SERVER['DOCUMENT_ROOT'] ."vendor/autoload.php";

$db = new Database();

$db->createTable('invoice_payment', [
    'id' => 'INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY',
    'invoice_id' => 'INT NOT NULL',
    'references_id' => 'VARCHAR(30) NULL',
    'item_name' => 'VARCHAR(255) NOT NULL',
    'payment_type' => 'VARCHAR(30) NOT NULL',
    'amount' => 'DECIMAL(13,2) NOT NULL',
    'customer_name' => 'VARCHAR(255) NOT NULL',
    'merchant_id' => 'VARCHAR(20) NOT NULL',
    'status' => 'VARCHAR(20) NOT NULL',
]);

$db->close();

echo "Successfull running migration create invoice_payment tables";
// php src/migrations/invoice_payment_migrations.php