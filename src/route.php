<?php

namespace Asdan\DetikPhpNative;

use Asdan\DetikPhpNative\Api\Pay;

class Route
{
    public function execute()
    {
        $method = $_SERVER['REQUEST_METHOD'];

        switch ($method) {
            case 'GET':
                $data = (object)$_GET;
                $pay = new Pay($data);
                $pay->checkStatus();
                break;
            case 'POST':
                $data = json_decode(file_get_contents('php://input'));
                $pay = new Pay($data);
                $pay->createTransaction();
                break;
            default:
                $this->notFound();
                break;
        }
    }

    public function notFound()
    {
        http_response_code(404);
        echo json_encode(array('message' => 'Endpoint tidak ditemukan'));
    }
}

